**Light weight cellular Materials (floating Materials)**

|Discipline|Physics|
| :- | :- |
|Theme|Material Science and Engineering|
|Module|Light weight cellular Materials|


**About the module:**

Light weight cellular material 

**Target Audience:**

Class 8 to 12

**Course alignment:**

Physics, Chemistry, Material Science 

**Boards mapped:**

CBSE, ISCE, State Boards

**Language:**

English


|Name of Developer/ principal investigator|Dr. Satanand Mishra|
| :- | :- |
|Institute|CSIR-AMPRI|
|Email. ID|snmishra07@gmail.com|
|Department|CARS & GM|

Contributor List

|Sr. No.|Name |Department|Institute|Email id|
| :- | :- | :- | :- | :- |
|1|Dr. D.P. Mondal|<p>ALLOYS, COMPOSITES AND CELLULAR MATERIALS</p><p></p>|CSIR-AMPRI, Bhopal|dpmondal@ampri.res.in|
|2|Dr. Gaurav Gupta|<p>ALLOYS, COMPOSITES AND CELLULAR MATERIALS</p><p></p>|CSIR-AMPRI, Bhopal|` `gauravkgupta@ampri.res.in|

